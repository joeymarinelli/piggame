package edu.westga.cs6910.pig.model.strategies;

/**
 * This is the advanced strategy class. It will implement the pig strategy
 * interface and allow the computer to play with a more advanced strategy
 * 
 * @author Joey Marinelli
 * @version 7/8/18
 *
 */
public class AdvancedStrategy implements PigStrategy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.westga.cs6910.pig.model.strategies.PigStrategy#rollAgain(int, int,
	 * int, int)
	 */
	@Override
	public boolean rollAgain(int numberOfRollsSoFar, int pointsSoFarThisTurn, int pointsToGoal,
			int opponentPointsToGoal) {
		double humanRollsLeft = Math.ceil((double) opponentPointsToGoal / 7);
		double computerRollsLeft = Math.ceil((double) pointsToGoal / 7);
		double averagePointsPerRollThisTurn = (double) pointsSoFarThisTurn / numberOfRollsSoFar;

		return !((pointsToGoal <= 0 || opponentPointsToGoal <= 0 || computerRollsLeft < humanRollsLeft)
				|| (computerRollsLeft == humanRollsLeft && averagePointsPerRollThisTurn >= 7 && opponentPointsToGoal > 7));

	}
}

package edu.westga.cs6910.pig.testing.advancedStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import edu.westga.cs6910.pig.model.strategies.AdvancedStrategy;

/**
 * This is the test class for the rollAgain method of the AdvancedStrategy
 * class.
 * 
 * @author Joey Marinelli
 * @version 7/8/18
 *
 */

public class AdvancedStrategyRollAgain {
	private AdvancedStrategy testStrategy;

	/**
	 * This is the setUp method that will initialize the AdvancedStraategy object
	 * before each test method is run.
	 */
	@BeforeEach
	public void setUp() {
		this.testStrategy = new AdvancedStrategy();
	}

	/**
	 * This test will confirm that rollAgain returns true at the beginning of the
	 * game when the computer plays first.
	 */
	@Test
	public void testShouldReturnTrueAtStartOfGame() {
		boolean result = this.testStrategy.rollAgain(0, 0, 100, 100);
		assertEquals(true, result);
	}

	/**
	 * This test will confirm that rollAgain returns true at the beginning of the
	 * game when the human plays first and gets no points.
	 */
	@Test
	public void testShouldReturnTrueAtStartOfGameAfterHumanPlayerHasGoneWithNoPoints() {
		boolean result = this.testStrategy.rollAgain(0, 0, 100, 100);
		assertEquals(true, result);
	}

	/**
	 * This test will confirm that rollAgain returns true at the beginning of the
	 * game when the human plays first and gets seven points.
	 */
	@Test
	public void testShouldReturnTrueAtStartOfGameAfterHumanPlayerHasGoneWithSomePoints() {
		boolean result = this.testStrategy.rollAgain(0, 0, 100, 93);
		assertEquals(true, result);
	}
	
	/**
	 * This test will confirm that the player will not roll again when it has 0 points left to reach the goal
	 */
	@Test
	public void testShouldReturnFalseWhenPlayerHasExactly0PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, 0, 93);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that the player will not roll again when it has less than 0 points left to reach the goal
	 */
	@Test
	public void testShouldReturnFalseWhenPlayerHasLessThan0PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, -3, 93);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that the player will not roll again when the human player has 0 points left to reach the goal
	 */
	@Test
	public void testShouldReturnFalseWhenHumanPlayerHasExactly0PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, 93, 0);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that the player will not roll again when the human player has less than 0 points left to reach the goal
	 */
	@Test
	public void testShouldReturnFalseWhenHumanPlayerHasLessThan0PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, 93, -3);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that the computer will roll again if the computer has exactly 7 points left to reach its goal
	 */
	@Test
	public void testShouldReturnTrueWhenHumanPlayerHasExactly7PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, 93, 7);
		assertEquals(true, result);
	}
	
	/**
	 * This test will confirm that the computer will roll again if the computer has less than 7 points left to reach its goal
	 */
	@Test
	public void testShouldReturnTrueWhenHumanPlayerHasLessThan7PointsToGoal() {
		boolean result = this.testStrategy.rollAgain(1, 7, 93, 5);
		assertEquals(true, result);
	}
	
	/**
	 * This test will confirm that the computer will roll again if the human has less rolls left than the computer
	 */
	@Test
	public void testShouldReturnTrueWhenHumanPlayerHasLessRollsLeftThanComputer() {
		boolean result = this.testStrategy.rollAgain(1, 7, 22, 20);
		assertEquals(true, result);
	}
	
	/**
	 * This test will confirm that the computer will not roll again if the computer has less rolls left than the human
	 */
	@Test
	public void testShouldReturnFalseWhenComputerPlayerHasLessRollsLeftThanHuman() {
		boolean result = this.testStrategy.rollAgain(1, 7, 20, 22);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that when the human and computer have the same number of rolls left and the computers
	 * average points this turn is less than 7, rollAgain will return true
	 */
	@Test
	public void testShouldReturnTrueWhenRollsLeftIsEqualAndComputerAveragePointsThisTurnIsLessThan7() {
		boolean result = this.testStrategy.rollAgain(3, 15, 28, 23);
		assertEquals(true, result);
	}
	
	/**
	 * This test will confirm that when the human and computer have the same number of rolls left and the computers
	 * average points this turn is exactly 7, rollAgain will return false
	 */
	@Test
	public void testShouldReturnFalseWhenRollsLeftIsEqualAndComputerAveragePointsThisTurnIsExactly7() {
		boolean result = this.testStrategy.rollAgain(2, 14, 28, 23);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that when the human and computer have the same number of rolls left and the computers
	 * average points this turn is more than 7, rollAgain will return false
	 */
	@Test
	public void testShouldReturnFalseWhenRollsLeftIsEqualAndComputerAveragePointsThisTurnIsMoreThan7() {
		boolean result = this.testStrategy.rollAgain(3, 24, 23, 22);
		assertEquals(false, result);
	}
	
	/**
	 * This test will confirm that the computer will roll again when both players are within 7 points of winning and the
	 * computer has more than 7 points per roll. 
	 */
	@Test
	public void testShouldReturnTrueWhenBothPlayersHave1RollLeftAndComputerHasMoreThanSevenPerRoll() {
		boolean result = this.testStrategy.rollAgain(3, 22, 5, 5);
		assertEquals(true, result);
	}
}
